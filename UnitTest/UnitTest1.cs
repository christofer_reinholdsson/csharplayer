﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Simple_Music_Player.MP3Files;
using System.IO;

namespace UnitTest
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void TestMp3TagReader()
        {
            string filename = @"C:\test\TestTrack.mp3";
    
            Mp3Tag mp3tag = new Mp3Tag(filename);
            Assert.AreEqual("Test Track", mp3tag.Title, true);
            Assert.AreEqual("Test Album", mp3tag.Album, true);
            StringAssert.Contains(mp3tag.Comment, "Test Comment");
            Assert.AreEqual( "Test Artist", mp3tag.Artist, true);
            Assert.AreEqual("2013", mp3tag.Year, true);
        }

        /// <summary>
        /// For this test to pass, an valid mp3 file must be present in c:\test
        /// </summary>
        [TestMethod]
        public void testMp3Validator()
        {
            string filename = @"C:\test\TestTrack.mp3";
            Assert.AreEqual(true, Mp3Tag.isValidMp3File(filename));
        }
    }
}
