﻿/* Class made by Christofer Reinholdsson, AC7626
 */
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Threading;

namespace Simple_Music_Player
{
    /// <summary>
    /// This class extends the TreeView by adding path to the root directory.
    /// Also contains methods to populate the tree by traversing the file-tree from
    /// some root folder down to the lowest path.
    /// </summary>
    public partial class FileExplorer : TreeView
    {
        // Root directory the tree is built from.
        private string m_currentRoot;

        // The directory currenty selected in the treeview (full path)
        private string m_selectedDir;

        /// <summary>
        /// Property to set and get the Root directory. The path set can not be null. Is
        /// trimmed from whitespaces leading and trailing.
        /// </summary>
        public string RootDir
        {
            get { return m_currentRoot; }
            set
            {
                if (value != null && value.Trim().Length > 0)
                {
                    m_currentRoot = value;
                    this.populateTree();
                }
                else
                    m_currentRoot = string.Empty;

            }
        }

        /// <summary>
        /// Property for the selected directory. Can not be set to null, instead 
        /// is set to string.empty.
        /// </summary>
        public string SelectedDir
        {
            get { return m_selectedDir; }
            set
            {
                if (value != null)
                    m_selectedDir = value;
                else
                    m_selectedDir = string.Empty;
            }
        }

        /// <summary>
        /// Default constructor for a file explorer. Sets the rootdir to string empty and initializes
        /// the selected dir to empty.
        /// </summary>
        public FileExplorer()
        {
            InitializeComponent();
            SelectedDir = string.Empty;
            RootDir = string.Empty;
        }


        /// <summary>
        /// Method to populate the tree from the root and downwards. Adding nodes
        /// to the tree in their according levels.
        /// </summary>
        protected void populateTree()
        {
            this.Nodes.Clear();
            DirectoryInfo curdir = new DirectoryInfo(RootDir);
            this.Nodes.Add(createDirectoryTree(curdir));
        }

        /// <summary>
        /// Recursive method to populate the tree. Traverses the file tree and adds each folder as it go.
        /// </summary>
        /// <param name="root">DirectoryInfo for the current root directory</param>
        /// <returns>A folder representing a root.</returns>
        private Folder createDirectoryTree(DirectoryInfo root)
        {
            Folder currentDirectory = new Folder(root.Name, root.FullName);
            foreach (DirectoryInfo dir in root.GetDirectories())
            {
                currentDirectory.Nodes.Add(createDirectoryTree(dir));
            }
            return currentDirectory;
        }

        /// <summary>
        /// Method to handle the event when the user has selected an item in the treeview.
        /// Sets the selected dir to the directory selected.
        /// </summary>
        /// <param name="e">Details associated with the event.</param>
        protected override void OnAfterSelect(TreeViewEventArgs e)
        {
            SelectedDir = ((Folder)this.SelectedNode).Path;
        }

        /// <summary>
        /// Method to handle the onpaint event, calls the base class onPaint.
        /// </summary>
        /// <param name="pe">Details associated with the event.</param>
        protected override void OnPaint(PaintEventArgs pe)
        {
            base.OnPaint(pe);
        }
    }
}
