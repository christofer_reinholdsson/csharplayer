﻿/* Class made by Claes Barthelson; AB1170
 */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace Simple_Music_Player.MP3Files
{
    /// <summary>
    /// This class holds lists for the songs to be played both
    /// in default order and random order as well as methods to
    /// add files to the lists.
    /// Also holds methods to play, pause, stop and skip songs
    /// in the lists.
    /// </summary>
    class Mp3FileHandler
    {

        public enum PlayStates { Playing, Stopped, Paused };

        private List<Mp3File> m_FileList;
        private LinkedList<int> m_RandomPlayOrder;
        private Mp3File m_currentlyPlaying;

        // Media player object to handle playback and let us use related controls
        WMPLib.WindowsMediaPlayer Player;

        DirectoryInfo m_dirInfo;
        private bool m_isRandomEnabled;
        private bool m_isRepeatEnabled;
        private PlayStates m_playState;
        
        /// <summary>
        /// Bool property to be set when the play order is
        /// switched to or from random, also generates a random
        /// play order if the property is set to true.
        /// </summary>
        public bool IsRandomEnabled
        {
            get { return m_isRandomEnabled; }
            set
            {
                if (value == true)
                {
                    m_isRandomEnabled = true;
                    generateRandomPlayOrder();
                }
                else
                    m_isRandomEnabled = false;
            }
        }

        /// <summary>
        /// Bool property to be set when switching repeat on and off.
        /// </summary>
        public bool IsRepeatEnabled
        {
            get { return m_isRepeatEnabled; }
            set { m_isRepeatEnabled = value; }
        }

        /// <summary>
        /// Index property of the song currently playing.
        /// </summary>
        public int CurrentSongIndex
        {
            get { return FileList.IndexOf(CurrentlyPlaying); }
        }
        
        /// <summary>
        /// String property that returns strings from the current 
        /// songs Tag object.
        /// </summary>
        public string CurrentSongString
        {
            get
            {
                return CurrentlyPlaying.Tag.Artist + " - " +
                CurrentlyPlaying.Tag.Title + " (" +
                CurrentlyPlaying.Tag.Album + ")";
            }
        }

        private LinkedList<int> RandomPlayOrder
        {
            get { return m_RandomPlayOrder; }
            set { m_RandomPlayOrder = value; }
        }

        /// <summary>
        /// PlayState property to be set when the current media
        /// changes state. eg Play, pause, stop, etc.
        /// </summary>
        public PlayStates PlayState
        {
            get { return m_playState; }
            set { m_playState = value; }
        }

        private List<Mp3File> FileList
        {
            get { return m_FileList; }
            set { m_FileList = value; }
        }

        public Mp3File CurrentlyPlaying
        {
            get { return m_currentlyPlaying; }
            set { m_currentlyPlaying = value; }
        }

        /// <summary>
        /// Property DirectoryInfo to hold info about a folder
        /// and its files.
        /// </summary>
        public DirectoryInfo DirInfo
        {
            get { return m_dirInfo; }
            set { m_dirInfo = value; }
        }


        public Mp3FileHandler(WMPLib.WindowsMediaPlayer mediaPlayer)
            : this(String.Empty, mediaPlayer)
        {
        }

        /// <summary>
        /// Constructor for the FileHandler.
        /// </summary>
        /// <param name="path">String path to the directory to be read.</param>
        /// <param name="mediaPlayer">WMP Object to handle player controls.</param>
        public Mp3FileHandler(String path, WMPLib.WindowsMediaPlayer mediaPlayer)
        {
            FileList = new List<Mp3File>();
            DirInfo = new DirectoryInfo(path);
            AddFiles(DirInfo);
            
            Player = mediaPlayer;
      
        }

        /// <summary>
        /// Adds files to the List of Mp3File objects. Also traverses down
        /// the directory tree to find subfolders and adds their items by
        /// a recursive call.
        /// </summary>
        /// <param name="directory"></param>
        private void AddFiles(DirectoryInfo directory)
        {
            foreach (DirectoryInfo dir in directory.GetDirectories())
            {
                AddFiles(dir);
            }

            foreach (FileInfo mp3File in directory.GetFiles("*.mp3"))
            {
                AddFile(mp3File.FullName);
            }
        }

        /// <summary>
        /// Stores the ToString() of all files in the file list in a String array.
        /// </summary>
        /// <returns>String array of file info.</returns>
        public String[] GetAllInfo()
        {
            String[] info = new String[FileList.Count];
            for (int i = 0; i < FileList.Count; i++)
                info[i] = String.Format("{0,-5}  {1}", i+1, FileList.ElementAt(i).ToString());
            return info;
        }

        /// <summary>
        /// Returns the ToString() value of the indexed object.
        /// </summary>
        /// <param name="index">Index to know what to return.</param>
        /// <returns>ToString() of wanted object.</returns>
        public String GetInfo(int index)
        {
            return FileList[index].ToString();
        }

        /// <summary>
        /// Add a file to the file list.
        /// </summary>
        /// <param name="fileName">Path to the file.</param>
        public void AddFile(String fileName)
        {
            if(Mp3Tag.isValidMp3File(fileName))
                FileList.Add(new Mp3File(fileName));
        }

        /// <summary>
        /// Remove a file from file list.
        /// </summary>
        /// <param name="index">Index of what to remove.</param>
        public void RemoveFile(int index)
        {
            FileList.RemoveAt(index);
        }

        /// <summary>
        /// If the index is in the list, set the CurrentlyPlaying and 
        /// PlayState to Playing, then set the WMP URL to the file path
        /// and start the song by calling the WMP.controls.play() method.
        /// </summary>
        /// <param name="index">Index of song to play.</param>
        public void Play(int index)
        {
            if (index >= 0 && index < FileList.Count)
            {
                CurrentlyPlaying = FileList.ElementAt(index);
                PlayState = PlayStates.Playing;
                Player.URL = CurrentlyPlaying.FileName;
                Player.controls.play();
            }
        }

        /// <summary>
        /// Set the CurrentlyPlaying accordingly, change the state and
        /// use the WMP.controls.stop() to stop the playback.
        /// </summary>
        public void Stop()
        {
            CurrentlyPlaying = null;
            PlayState = PlayStates.Stopped;
            Player.controls.stop();
        }

        /// <summary>
        /// Plays or pauses a song according to the current PlayState.
        /// If the state is Playing controls.pause() will be called.
        /// If the state is Paused controls.play() will be called.
        /// </summary>
        public void PauseResume()
        {
            if (PlayState==PlayStates.Playing)
            {
                PlayState = PlayStates.Paused;
                Player.controls.pause();
            }
            else
            {
                PlayState = PlayStates.Playing;
                Player.controls.play();
            }
        }

        /// <summary>
        /// Increment the index of CurrentlyPlaying by one and play that song.
        /// </summary>
        public void PlayNext()
        {
            if (IsRandomEnabled)
                playNextRandom();

            else if (CurrentSongIndex == FileList.Count - 1)
                if (IsRepeatEnabled)
                    Play(0);
                else
                    Stop();
            else
                Play(CurrentSongIndex + 1);
        }

        /// <summary>
        /// Decrement the index of CurrentlyPlaying by one and play that song.
        /// </summary>
        public void PlayPrev()
        {
            if (CurrentSongIndex == 0)
                Play(CurrentSongIndex);
            else
                Play(CurrentSongIndex - 1);            
        }


        /// <summary>
        /// Method to generate a random playorder without beginning at a certain song number
        /// </summary>
        private void generateRandomPlayOrder()
        {
            RandomPlayOrder = new LinkedList<int>();
            Random a = new Random();
            for (int i = 0; i < FileList.Count; i++)
            {
                int next = a.Next(FileList.Count);
                if (!RandomPlayOrder.Contains(next))
                    RandomPlayOrder.AddLast(next);
                else
                    i--; // to make this iteration of the loop not count
            }
        }

        /// <summary>
        /// Method to play next song in the random order playlist.
        /// </summary>
        private void playNextRandom()
        {
            if (RandomPlayOrder.Count > 0)
            {
                Play(RandomPlayOrder.First.Value);
                RandomPlayOrder.RemoveFirst();
            }
            else
            {
                if (IsRepeatEnabled)
                {
                    generateRandomPlayOrder();
                    playNextRandom();
                }
                else
                    Stop();
            }
        }

    }
}
