﻿/* Class made by Claes Barthelson; AB1170
 */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Simple_Music_Player.MP3Files
{
    /// <summary>
    /// This class holds the file name and file tag of an
    /// mp3 file and is used in the Mp3FileHandler class.
    /// </summary>
    public class Mp3File
    {
        private String m_fileName;
        private Mp3Tag m_fileTag;

        public String FileName
        {
            get { return m_fileName; }
            private set { m_fileName = value; }
        }

        public Mp3Tag Tag
        {
            get { return m_fileTag; }
            private set { m_fileTag = value; }
        }

        /// <summary>
        /// Constructor for the Mp3File. Checks is string is empty and
        /// if the file holds a valid ID3v1 tag before creating the 
        /// Mp3Tag object.
        /// </summary>
        /// <param name="filename">Path to the mp3 file.</param>
        public Mp3File(String filename)
        {
            if (!String.IsNullOrWhiteSpace(filename))
            {
                FileName = filename;
                Tag = new Mp3Tag(filename);
                
            }
        }

        /// <summary>
        /// Private method to validate that a string is not null. If value is not null, it 
        /// is returned. Otherwise string.empty is returned.
        /// </summary>
        /// <param name="value">The string to check.</param>
        /// <returns>A string that is not null.</returns>
        private string stringNotNull(string value)
        {
            if (value!=null)
                return value;
            else
                return string.Empty;
        }

        /// <summary>
        /// Override of this class' ToString()-method, so that it returns the Tag objects
        /// ToString().
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return Tag.ToString();
        }
    }
}
