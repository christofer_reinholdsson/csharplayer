﻿/* Class made by Christofer Reinholdsson, AC7626
 */
using Microsoft.Win32.SafeHandles;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Simple_Music_Player.MP3Files
{
    /// <summary>
    /// This class only support the reading of ID3v1 tags, and works according to the ID3v1
    /// standard, where tags is organized in this fashion
    /// 
    ///    Field    Bytes
    ///    ----------------
    ///    'TAG'    0-3
    ///    title    3-32
    ///    artist   33-62
    ///    album    63-92
    ///    year     93-96
    ///    comments 97-126
    ///    genre    127
    /// </summary>
    public class Mp3Tag
    {
        /// <summary>
        /// Static constants regarding offsets from end for all the fields.
        /// </summary>
        static private readonly int artistOffsetFromEnd = 125;
        static private readonly int titleOffsetFromEnd = 95;
        static private readonly int albumoffsetFromEnd = 65;
        static private readonly int yearOffsetFromEnd = 35;
        static private readonly int commentOffsetFromEnd = 31;
        static private readonly int genreOffsetFromEnd = 1;
        static private readonly int totalSizeID3v1 = 128;

        private string m_title;
        private string m_artist;
        private string m_album;
        private string m_year;
        private string m_comment;
        private string m_genre;

        /// <summary>
        /// Property for the title. Get works as expected, if set is given an
        /// null value, String.empty is assigned instead.
        /// </summary>
        public string Title
        {
            get { return m_title; }
            set { m_title = stringNotNull(value); }
        }

        /// <summary>
        /// Property for the artist. Get works as expected, if set is given an
        /// null value, String.empty is assigned instead.
        /// </summary>
        public string Artist
        {
            get { return m_artist; }
            set { m_artist = this.stringNotNull(value); }
        }

        /// <summary>
        /// Property for the album. Get works as expected, if set is given an
        /// null value, String.empty is assigned instead.
        /// </summary>
        public string Album
        {
            get { return m_album; }
            set { m_album = this.stringNotNull(value); }
        }

        /// <summary>
        /// Property for the year. Get works as expected, if set is given an
        /// null value, String.empty is assigned instead.
        /// </summary>
        public string Year
        {
            get { return m_year; }
            set { m_year = this.stringNotNull(value); }
        }

        /// <summary>
        /// Property for the comment. Get works as expected, if set is given an
        /// null value, String.empty is assigned instead.
        /// </summary>
        public string Comment
        {
            get { return m_comment; }
            set { m_comment = this.stringNotNull(value); }
        }

        /// <summary>
        /// Property for the genre. Get works as expected, if set is given an
        /// null value, String.empty is assigned instead.
        /// </summary>
        public string Genre
        {
            get { return m_genre; }
            set { m_genre = this.stringNotNull(value); }
        }

        /// <summary>
        /// Constructor for a new tag. Takes a full path to an MP3-file and tries to read
        /// all tag information from that file. If reading is not successfull, all fields is
        /// set to string.empty. The idea is to call the static method Mp3Tag.isValidMp3(filename)
        /// before creating an instance.
        /// </summary>
        /// <param name="pathToMp3File">Full path to the mp3-file to be read.</param>
        public Mp3Tag(string pathToMp3File)
        {
            if (!this.tryParse(pathToMp3File))
            {
                Artist=string.Empty;
                Title=string.Empty;
                Album=string.Empty;
            }
        }

        /// <summary>
        /// Private method to validate that a string is not null. If value is not null, it 
        /// is returned. Otherwise string.empty is returned.
        /// </summary>
        /// <param name="value">The string to check.</param>
        /// <returns>A string that is not null.</returns>
        private string stringNotNull(string value)
        {
            if (value!=null)
                return value;
            else
                return string.Empty;
        }

        /// <summary>
        /// Method that tries to parse all fields in a mp3-file. If all fields is successfully
        /// read (even if they are empty) this function is seen as successfull.
        /// </summary>
        /// <param name="pathToMp3File">Full path to the mp3-file to parse</param>
        /// <returns>True if parse is successfull for all fields, else false.</returns>
        private bool tryParse(string pathToMp3File)
        {
            FileStream fileStream = File.OpenRead(pathToMp3File);
            bool isTitleOk = parseTitle(fileStream);
            bool isArtistOk = parseArtist(fileStream);
            bool isAlbumOk = parseAlbum(fileStream);
            bool isYearOk = parseYear(fileStream);
            bool isCommentOk = parseComment(fileStream);
            bool isGenreOk=parseGenre(fileStream);
            return isTitleOk && isArtistOk && isAlbumOk && isYearOk && isCommentOk && isGenreOk;
        }

        /// <summary>
        /// Method to parse the title field of a mp3-file. If the field is successfully read
        /// (even if it is empty) the parse is seen as successfull.
        /// </summary>
        /// <param name="mp3FileStream">An open FileStream object to a MP3-file</param>
        /// <returns>True if parse is successfull, else false</returns>
        private bool parseTitle(FileStream mp3FileStream)
        {
            try
            {
                Title = readAndEncode(mp3FileStream, artistOffsetFromEnd, 30);
                return true;
            }
            catch (Exception e)
            {
                Title = string.Empty;
                return false;
            }
        }

        /// <summary>
        /// Method to parse the title field of a mp3-file. If the field is successfully read
        /// (even if it is empty) the parse is seen as successfull. 
        /// </summary>
        /// <param name="mp3FileStream">An open FileStream object to a MP3-file</param>
        /// <returns>True if parse is successfull, else false</returns>
        private bool parseArtist(FileStream mp3FileStream)
        {
            try
            {
                Artist = readAndEncode(mp3FileStream, titleOffsetFromEnd, 30);
                return true;
            }
            catch (Exception e)
            {
                Artist = string.Empty;
                return false;
            }
        }

        /// <summary>
        ///  Method to parse the title field of a mp3-file. If the field is successfully read
        /// (even if it is empty) the parse is seen as successfull.
        /// </summary>
        /// <param name="mp3FileStream">An open FileStream object to a MP3-file</param>
        /// <returns>True if parse is successfull, else false</returns>
        private bool parseAlbum(FileStream mp3FileStream)
        {
            try
            {
                Album = readAndEncode(mp3FileStream, albumoffsetFromEnd, 30);
                return true;
            }
            catch (Exception e)
            {
                Album = string.Empty;
                return false;
            }
        }

        /// <summary>
        ///  Method to parse the title field of a mp3-file. If the field is successfully read
        /// (even if it is empty) the parse is seen as successfull.
        /// </summary>
        /// <param name="mp3FileStream">An open FileStream object to a MP3-file</param>
        /// <returns>True if parse is successfull, else false</returns>
        private bool parseYear(FileStream mp3FileStream)
        {
            try
            {
                Year = readAndEncode(mp3FileStream, yearOffsetFromEnd, 4);
                return true;
            }
            catch (Exception e)
            {
                Year = string.Empty;
                return false;
            }
        }

        /// <summary>
        ///  Method to parse the title field of a mp3-file. If the field is successfully read
        /// (even if it is empty) the parse is seen as successfull.
        /// </summary>
        /// <param name="mp3FileStream">An open FileStream object to a MP3-file</param>
        /// <returns>True if parse is successfull, else false</returns>
        private bool parseComment(FileStream mp3FileStream)
        {
            try
            {
                Comment = readAndEncode(mp3FileStream, commentOffsetFromEnd, 30);
                return true;
            }
            catch (Exception e)
            {
                Comment = string.Empty;
                return false;
            }
        }

        /// <summary>
        ///  Method to parse the title field of a mp3-file. If the field is successfully read
        /// (even if it is empty) the parse is seen as successfull.
        /// </summary>
        /// <param name="mp3FileStream">An open FileStream object to a MP3-file</param>
        /// <returns>True if parse is successfull, else false</returns>
        private bool parseGenre(FileStream mp3FileStream)
        {
            try
            {
                Genre = readAndEncode(mp3FileStream, genreOffsetFromEnd, 1);
                return true;
            }

            catch (Exception e)
            {
                Genre = string.Empty;
                return false;
            }
        }

        /// <summary>
        /// Method to validate that a file does contain a valid ID3v1 tag. A valid ID3v1 tag
        /// consists of 128 bytes appended to the end of a file, where the first three
        /// bytes consists of the letters "TAG". 
        /// </summary>
        /// <param name="fullPathToMp3">Full path to mp3-file to check.</param>
        /// <returns>True if there is a valid ID3v1 tag in that file, else false.</returns>
        public static bool isValidMp3File(string fullPathToMp3)
        {
            try
            {
                FileStream mp3FileStream = File.OpenRead(fullPathToMp3);
                string tag = readAndEncode(mp3FileStream, totalSizeID3v1, 3);
                if (tag.Equals("TAG", StringComparison.CurrentCultureIgnoreCase))
                    return true;
                return false;
            }
            catch (Exception e)
            {
                return false;
            }
        }
                
        /// <summary>
        /// Method to read a given number of bytes from the filestream, starting at an offset
        /// from the end forward.
        /// </summary>
        /// <param name="mp3FileStream">Open FileStream object to the file to be read.</param>
        /// <param name="offsetFromEnd">Offset from the end where the first byte to be read is</param>
        /// <param name="numberOfBytes">Number of bytes to be read.</param>
        /// <returns>String containing the characters read, trimmed from '\0' characters.</returns>
        private static string readAndEncode(FileStream mp3FileStream, int offsetFromEnd, int numberOfBytes)
        {
            byte[] tagBytes = new byte[numberOfBytes];
            mp3FileStream.Seek(-offsetFromEnd, SeekOrigin.End);
            mp3FileStream.Read(tagBytes, 0, numberOfBytes);
            //string equivalentString = ASCIIEncoding.ASCII.GetString(tagBytes);
            //int nullByte = equivalentString.IndexOf('\0');
            return ASCIIEncoding.ASCII.GetString(tagBytes).Trim(new char[] { '\0' });
            //return nullByte>0 ? equivalentString.Substring(0, nullByte) : equivalentString;
        }


        /// <summary>
        /// Override of the toString function. Returns information about the tag,
        /// artist, album and title.
        /// </summary>
        /// <returns>Formatted string with information about the object.</returns>
        public override string ToString()
        {
            return string.Format("{0,-26}  {1,-26}  {2,-26}", stringCutter(this.Artist,26), 
                                                              stringCutter(this.Album,26), 
                                                              stringCutter(this.Title,26));
        }


        /// <summary>
        /// Method to cut strings that is too long. Cuts the string and adds "..." for strings
        /// that are cut.
        /// </summary>
        /// <param name="value">String to be evaluated</param>
        /// <param name="length">Maximum length for the string</param>
        /// <returns>String with a maxlength of length.</returns>
        private string stringCutter(string value, int length)
        {
            if (value.Length < length)
                return value;
            else return value.Substring(0, length - 4) + " ...";
        }
    }
}
