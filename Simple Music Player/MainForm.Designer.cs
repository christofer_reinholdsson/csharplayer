﻿namespace Simple_Music_Player
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBoxFolders = new System.Windows.Forms.GroupBox();
            this.buttonSetBaseDir = new System.Windows.Forms.Button();
            this.buttonPlaySelectedFolder = new System.Windows.Forms.Button();
            this.groupBoxNowPlaying = new System.Windows.Forms.GroupBox();
            this.labelPlayTime = new System.Windows.Forms.Label();
            this.trackBarMP3 = new System.Windows.Forms.TrackBar();
            this.labelNowPlaying = new System.Windows.Forms.Label();
            this.groupBoxControls = new System.Windows.Forms.GroupBox();
            this.labelShuffle = new System.Windows.Forms.Label();
            this.labelRepeat = new System.Windows.Forms.Label();
            this.labelVolume = new System.Windows.Forms.Label();
            this.trackBarVolume = new System.Windows.Forms.TrackBar();
            this.buttonNext = new System.Windows.Forms.Button();
            this.buttonStop = new System.Windows.Forms.Button();
            this.buttonPause = new System.Windows.Forms.Button();
            this.buttonPlay = new System.Windows.Forms.Button();
            this.buttonPrev = new System.Windows.Forms.Button();
            this.groupBoxPlaylist = new System.Windows.Forms.GroupBox();
            this.listBoxPlayList = new System.Windows.Forms.ListBox();
            this.fileExplorerDirectories = new Simple_Music_Player.FileExplorer();
            this.groupBoxFolders.SuspendLayout();
            this.groupBoxNowPlaying.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trackBarMP3)).BeginInit();
            this.groupBoxControls.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trackBarVolume)).BeginInit();
            this.groupBoxPlaylist.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBoxFolders
            // 
            this.groupBoxFolders.Controls.Add(this.fileExplorerDirectories);
            this.groupBoxFolders.Controls.Add(this.buttonSetBaseDir);
            this.groupBoxFolders.Controls.Add(this.buttonPlaySelectedFolder);
            this.groupBoxFolders.Location = new System.Drawing.Point(12, 12);
            this.groupBoxFolders.Name = "groupBoxFolders";
            this.groupBoxFolders.Size = new System.Drawing.Size(231, 431);
            this.groupBoxFolders.TabIndex = 0;
            this.groupBoxFolders.TabStop = false;
            this.groupBoxFolders.Text = "Folders";
            // 
            // buttonSetBaseDir
            // 
            this.buttonSetBaseDir.Location = new System.Drawing.Point(6, 397);
            this.buttonSetBaseDir.Name = "buttonSetBaseDir";
            this.buttonSetBaseDir.Size = new System.Drawing.Size(75, 23);
            this.buttonSetBaseDir.TabIndex = 1;
            this.buttonSetBaseDir.Text = "Select root";
            this.buttonSetBaseDir.UseVisualStyleBackColor = true;
            this.buttonSetBaseDir.Click += new System.EventHandler(this.buttonSetBaseDir_Click);
            // 
            // buttonPlaySelectedFolder
            // 
            this.buttonPlaySelectedFolder.Location = new System.Drawing.Point(89, 397);
            this.buttonPlaySelectedFolder.Name = "buttonPlaySelectedFolder";
            this.buttonPlaySelectedFolder.Size = new System.Drawing.Size(136, 23);
            this.buttonPlaySelectedFolder.TabIndex = 2;
            this.buttonPlaySelectedFolder.Text = "Play selected folder";
            this.buttonPlaySelectedFolder.UseVisualStyleBackColor = true;
            this.buttonPlaySelectedFolder.Click += new System.EventHandler(this.buttonPlaySelectedFolder_Click);
            // 
            // groupBoxNowPlaying
            // 
            this.groupBoxNowPlaying.Controls.Add(this.labelPlayTime);
            this.groupBoxNowPlaying.Controls.Add(this.trackBarMP3);
            this.groupBoxNowPlaying.Controls.Add(this.labelNowPlaying);
            this.groupBoxNowPlaying.Location = new System.Drawing.Point(250, 12);
            this.groupBoxNowPlaying.Name = "groupBoxNowPlaying";
            this.groupBoxNowPlaying.Size = new System.Drawing.Size(612, 68);
            this.groupBoxNowPlaying.TabIndex = 3;
            this.groupBoxNowPlaying.TabStop = false;
            this.groupBoxNowPlaying.Text = "Now playing";
            // 
            // labelPlayTime
            // 
            this.labelPlayTime.AutoSize = true;
            this.labelPlayTime.Location = new System.Drawing.Point(521, 20);
            this.labelPlayTime.Name = "labelPlayTime";
            this.labelPlayTime.Size = new System.Drawing.Size(0, 13);
            this.labelPlayTime.TabIndex = 2;
            // 
            // trackBarMP3
            // 
            this.trackBarMP3.AutoSize = false;
            this.trackBarMP3.Location = new System.Drawing.Point(6, 37);
            this.trackBarMP3.Margin = new System.Windows.Forms.Padding(3, 0, 3, 0);
            this.trackBarMP3.Name = "trackBarMP3";
            this.trackBarMP3.Size = new System.Drawing.Size(600, 26);
            this.trackBarMP3.TabIndex = 1;
            this.trackBarMP3.TickStyle = System.Windows.Forms.TickStyle.None;
            this.trackBarMP3.MouseCaptureChanged += new System.EventHandler(this.trackBarMP3_MouseCaptureChanged);
            // 
            // labelNowPlaying
            // 
            this.labelNowPlaying.AutoSize = true;
            this.labelNowPlaying.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelNowPlaying.Location = new System.Drawing.Point(6, 16);
            this.labelNowPlaying.MaximumSize = new System.Drawing.Size(600, 0);
            this.labelNowPlaying.Name = "labelNowPlaying";
            this.labelNowPlaying.Size = new System.Drawing.Size(141, 18);
            this.labelNowPlaying.TabIndex = 0;
            this.labelNowPlaying.Text = "Nothing is playing";
            // 
            // groupBoxControls
            // 
            this.groupBoxControls.Controls.Add(this.labelShuffle);
            this.groupBoxControls.Controls.Add(this.labelRepeat);
            this.groupBoxControls.Controls.Add(this.labelVolume);
            this.groupBoxControls.Controls.Add(this.trackBarVolume);
            this.groupBoxControls.Controls.Add(this.buttonNext);
            this.groupBoxControls.Controls.Add(this.buttonStop);
            this.groupBoxControls.Controls.Add(this.buttonPause);
            this.groupBoxControls.Controls.Add(this.buttonPlay);
            this.groupBoxControls.Controls.Add(this.buttonPrev);
            this.groupBoxControls.Location = new System.Drawing.Point(250, 86);
            this.groupBoxControls.Name = "groupBoxControls";
            this.groupBoxControls.Size = new System.Drawing.Size(612, 58);
            this.groupBoxControls.TabIndex = 4;
            this.groupBoxControls.TabStop = false;
            this.groupBoxControls.Text = "Controls";
            // 
            // labelShuffle
            // 
            this.labelShuffle.Image = global::Simple_Music_Player.Properties.Resources.Shuffle_disabled;
            this.labelShuffle.Location = new System.Drawing.Point(521, 12);
            this.labelShuffle.Name = "labelShuffle";
            this.labelShuffle.Size = new System.Drawing.Size(42, 40);
            this.labelShuffle.TabIndex = 10;
            this.labelShuffle.Click += new System.EventHandler(this.labelShuffle_Click);
            // 
            // labelRepeat
            // 
            this.labelRepeat.Image = global::Simple_Music_Player.Properties.Resources.Repeat_Disabled;
            this.labelRepeat.Location = new System.Drawing.Point(562, 15);
            this.labelRepeat.Name = "labelRepeat";
            this.labelRepeat.Size = new System.Drawing.Size(44, 33);
            this.labelRepeat.TabIndex = 9;
            this.labelRepeat.Click += new System.EventHandler(this.labelRepeat_Click);
            // 
            // labelVolume
            // 
            this.labelVolume.Image = global::Simple_Music_Player.Properties.Resources.Volume_Full;
            this.labelVolume.Location = new System.Drawing.Point(462, 12);
            this.labelVolume.Name = "labelVolume";
            this.labelVolume.Size = new System.Drawing.Size(42, 24);
            this.labelVolume.TabIndex = 8;
            // 
            // trackBarVolume
            // 
            this.trackBarVolume.AutoSize = false;
            this.trackBarVolume.Location = new System.Drawing.Point(448, 35);
            this.trackBarVolume.Margin = new System.Windows.Forms.Padding(0);
            this.trackBarVolume.Maximum = 100;
            this.trackBarVolume.Name = "trackBarVolume";
            this.trackBarVolume.Size = new System.Drawing.Size(70, 20);
            this.trackBarVolume.TabIndex = 7;
            this.trackBarVolume.TickStyle = System.Windows.Forms.TickStyle.None;
            this.trackBarVolume.Value = 100;
            this.trackBarVolume.Scroll += new System.EventHandler(this.trackBarVolume_Scroll);
            // 
            // buttonNext
            // 
            this.buttonNext.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.buttonNext.FlatAppearance.BorderSize = 0;
            this.buttonNext.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonNext.Image = global::Simple_Music_Player.Properties.Resources.Next_Track;
            this.buttonNext.Location = new System.Drawing.Point(230, 12);
            this.buttonNext.Margin = new System.Windows.Forms.Padding(0);
            this.buttonNext.Name = "buttonNext";
            this.buttonNext.Size = new System.Drawing.Size(62, 39);
            this.buttonNext.TabIndex = 4;
            this.buttonNext.UseVisualStyleBackColor = true;
            this.buttonNext.Click += new System.EventHandler(this.buttonNext_Click);
            // 
            // buttonStop
            // 
            this.buttonStop.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.buttonStop.FlatAppearance.BorderSize = 0;
            this.buttonStop.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonStop.Image = global::Simple_Music_Player.Properties.Resources.Stop;
            this.buttonStop.Location = new System.Drawing.Point(186, 12);
            this.buttonStop.Name = "buttonStop";
            this.buttonStop.Size = new System.Drawing.Size(41, 39);
            this.buttonStop.TabIndex = 3;
            this.buttonStop.UseVisualStyleBackColor = true;
            this.buttonStop.Click += new System.EventHandler(this.buttonStop_Click);
            // 
            // buttonPause
            // 
            this.buttonPause.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.buttonPause.FlatAppearance.BorderSize = 0;
            this.buttonPause.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonPause.Image = global::Simple_Music_Player.Properties.Resources.Pause;
            this.buttonPause.Location = new System.Drawing.Point(138, 16);
            this.buttonPause.Name = "buttonPause";
            this.buttonPause.Size = new System.Drawing.Size(42, 33);
            this.buttonPause.TabIndex = 2;
            this.buttonPause.UseVisualStyleBackColor = true;
            this.buttonPause.Click += new System.EventHandler(this.buttonPause_Click);
            // 
            // buttonPlay
            // 
            this.buttonPlay.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.buttonPlay.FlatAppearance.BorderSize = 0;
            this.buttonPlay.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonPlay.Image = global::Simple_Music_Player.Properties.Resources.Play;
            this.buttonPlay.Location = new System.Drawing.Point(97, 16);
            this.buttonPlay.Name = "buttonPlay";
            this.buttonPlay.Size = new System.Drawing.Size(35, 33);
            this.buttonPlay.TabIndex = 1;
            this.buttonPlay.UseVisualStyleBackColor = true;
            this.buttonPlay.Click += new System.EventHandler(this.buttonPlay_Click);
            // 
            // buttonPrev
            // 
            this.buttonPrev.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.buttonPrev.FlatAppearance.BorderSize = 0;
            this.buttonPrev.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonPrev.Image = global::Simple_Music_Player.Properties.Resources.prevTrack;
            this.buttonPrev.Location = new System.Drawing.Point(28, 15);
            this.buttonPrev.Name = "buttonPrev";
            this.buttonPrev.Size = new System.Drawing.Size(63, 33);
            this.buttonPrev.TabIndex = 0;
            this.buttonPrev.UseVisualStyleBackColor = true;
            this.buttonPrev.Click += new System.EventHandler(this.buttonPrev_Click);
            // 
            // groupBoxPlaylist
            // 
            this.groupBoxPlaylist.Controls.Add(this.listBoxPlayList);
            this.groupBoxPlaylist.Location = new System.Drawing.Point(250, 150);
            this.groupBoxPlaylist.Name = "groupBoxPlaylist";
            this.groupBoxPlaylist.Size = new System.Drawing.Size(612, 293);
            this.groupBoxPlaylist.TabIndex = 5;
            this.groupBoxPlaylist.TabStop = false;
            this.groupBoxPlaylist.Text = "Playlist";
            // 
            // listBoxPlayList
            // 
            this.listBoxPlayList.AllowDrop = true;
            this.listBoxPlayList.Font = new System.Drawing.Font("Lucida Console", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.listBoxPlayList.FormattingEnabled = true;
            this.listBoxPlayList.ItemHeight = 11;
            this.listBoxPlayList.Location = new System.Drawing.Point(6, 19);
            this.listBoxPlayList.Name = "listBoxPlayList";
            this.listBoxPlayList.Size = new System.Drawing.Size(600, 268);
            this.listBoxPlayList.TabIndex = 0;
            this.listBoxPlayList.SelectedIndexChanged += new System.EventHandler(this.listBoxPlayList_SelectedIndexChanged);
            this.listBoxPlayList.DragDrop += new System.Windows.Forms.DragEventHandler(this.listBoxPlayList_DragDrop);
            this.listBoxPlayList.DragEnter += new System.Windows.Forms.DragEventHandler(this.listBoxPlayList_DragEnter);
            this.listBoxPlayList.DoubleClick += new System.EventHandler(this.listBoxPlayList_DoubleClick);
            // 
            // fileExplorerDirectories
            // 
            this.fileExplorerDirectories.Location = new System.Drawing.Point(6, 13);
            this.fileExplorerDirectories.Name = "fileExplorerDirectories";
            this.fileExplorerDirectories.RootDir = "";
            this.fileExplorerDirectories.SelectedDir = "";
            this.fileExplorerDirectories.Size = new System.Drawing.Size(219, 378);
            this.fileExplorerDirectories.TabIndex = 0;
            // 
            // MainForm
            // 
            this.AllowDrop = true;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(874, 455);
            this.Controls.Add(this.groupBoxPlaylist);
            this.Controls.Add(this.groupBoxControls);
            this.Controls.Add(this.groupBoxNowPlaying);
            this.Controls.Add(this.groupBoxFolders);
            this.Name = "MainForm";
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.Text = "CSharPlayer";
            this.DragDrop += new System.Windows.Forms.DragEventHandler(this.MainForm_DragEnter);
            this.DragEnter += new System.Windows.Forms.DragEventHandler(this.MainForm_DragEnter);
            this.groupBoxFolders.ResumeLayout(false);
            this.groupBoxNowPlaying.ResumeLayout(false);
            this.groupBoxNowPlaying.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trackBarMP3)).EndInit();
            this.groupBoxControls.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.trackBarVolume)).EndInit();
            this.groupBoxPlaylist.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBoxFolders;
        private System.Windows.Forms.Button buttonSetBaseDir;
        private System.Windows.Forms.Button buttonPlaySelectedFolder;
        private System.Windows.Forms.GroupBox groupBoxNowPlaying;
        private System.Windows.Forms.Label labelNowPlaying;
        private System.Windows.Forms.GroupBox groupBoxControls;
        private System.Windows.Forms.Button buttonNext;
        private System.Windows.Forms.Button buttonStop;
        private System.Windows.Forms.Button buttonPause;
        private System.Windows.Forms.Button buttonPlay;
        private System.Windows.Forms.Button buttonPrev;
        private System.Windows.Forms.GroupBox groupBoxPlaylist;
        private System.Windows.Forms.ListBox listBoxPlayList;
        private FileExplorer fileExplorerDirectories;
        private System.Windows.Forms.TrackBar trackBarMP3;
        private System.Windows.Forms.Label labelPlayTime;
        private System.Windows.Forms.Label labelVolume;
        private System.Windows.Forms.TrackBar trackBarVolume;
        private System.Windows.Forms.Label labelRepeat;
        private System.Windows.Forms.Label labelShuffle;
    }
}