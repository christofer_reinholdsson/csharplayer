﻿/* Class made by Claes Barthelson, AB1170 and Christofer Reinholdsson, AC7626
 */
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Simple_Music_Player.MP3Files;
using Microsoft.WindowsAPICodePack.Taskbar;

namespace Simple_Music_Player
{
    /// <summary>
    /// Main window.
    /// 
    /// Credits for all graphics goes to ipapun, and his awesome Devine Part 2 icon pack.
    /// http://ipapun.deviantart.com/
    /// 
    /// For this player to work, you must have windows media player installed.
    /// </summary>
    public partial class MainForm : Form
    {
        private Mp3FileHandler m_mp3FileHandler;
        string programName = "CSharPlayer";
        private WMPLib.WindowsMediaPlayer m_MPlayer;
        private Timer m_playNextTimer;
        private Timer m_trackBarTimer;
        private TaskbarManager m_taskbar = TaskbarManager.Instance;


        
        WMPLib.WindowsMediaPlayer MPlayer
        {
            get { return m_MPlayer; }
            set { m_MPlayer = value; }
        }

        Timer TimerPlayNext
        {
            get { return m_playNextTimer; }
            set { m_playNextTimer = value; }
        }

        Timer TimerTrackBar
        {
            get { return m_trackBarTimer; }
            set { m_trackBarTimer = value; }
        }

        Mp3FileHandler MP3FH
        {
            get { return m_mp3FileHandler; }
            set { m_mp3FileHandler = value; }
        }

        
        /// <summary>
        /// Default constructor for the mainform. Initializes the GUI, initializes the timer and initializes
        /// a new Windows Media Player object.
        /// 
        /// Adds events to the media player and the timer.
        /// </summary>
        public MainForm()
        {
            InitializeComponent();
            TimerPlayNext = new Timer();
            TimerTrackBar = new Timer();
            TimerTrackBar.Tick+=new EventHandler(TrackBarTimer_Tick);
            TimerTrackBar.Interval = 500;

            MPlayer = new WMPLib.WindowsMediaPlayer();
            MPlayer.PlayStateChange += new WMPLib._WMPOCXEvents_PlayStateChangeEventHandler(MPlayer_PlayStateChange);
            TimerPlayNext.Tick += new EventHandler(PlayNextTimer_Tick);
            
        }

        /// <summary>
        /// Method to handle the event caused by a timer tick.
        /// This timer is used to set the values of the trackbar
        /// and the label PlayTime.
        /// </summary>
        /// <param name="sender">The sending timer</param>
        /// <param name="e">Details concerning the event.</param>
        private void TrackBarTimer_Tick(object sender, EventArgs e)
        {
            if (MPlayer.playState == WMPLib.WMPPlayState.wmppsPlaying)
            {
                if (MPlayer.controls.currentPosition < trackBarMP3.Maximum)
                    trackBarMP3.Value = (int)MPlayer.controls.currentPosition;
                labelPlayTime.Text = String.Format("({0}/{1})",
                                                     MPlayer.controls.currentPositionString,
                                                     MPlayer.currentMedia.durationString);
                m_taskbar.SetProgressValue((int)MPlayer.controls.currentPosition, (int)MPlayer.currentMedia.duration);
            }
        }

        /// <summary>
        /// Method to handle the event caused by a timer tick. 
        /// </summary>
        /// <param name="sender">The sending timer</param>
        /// <param name="e">Details concerning the event.</param>
        void PlayNextTimer_Tick(object sender, EventArgs e)
        {

            MP3FH.PlayNext();
            UpdateGUI();
            TimerPlayNext.Enabled = false;
        }

        /// <summary>
        /// Method to handle the event when the user wants to set the root directory for
        /// the file explorer. Shows a FolderBrowserdialog.
        /// </summary>
        /// <param name="sender">The object calling the event handler</param>
        /// <param name="e">Details concerning the event.</param>
        private void buttonSetBaseDir_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog fbd = new FolderBrowserDialog();
            fbd.ShowDialog();
            if (fbd.SelectedPath != null)
            {
                fileExplorerDirectories.RootDir = fbd.SelectedPath;
            }

        }

        /// <summary>
        /// Method to handle the event when the user wants to play a selected
        /// folder in the filebrowser. Loads the files, selects the first and play
        /// it.
        /// </summary>
        /// <param name="sender">The object calling the event handler</param>
        /// <param name="e">Details concerning the event.</param>
        private void buttonPlaySelectedFolder_Click(object sender, EventArgs e)
        {
            if (fileExplorerDirectories.SelectedDir != string.Empty)
            {
                this.loadDirs();
                if (listBoxPlayList.Items.Count > 0)
                {
                    MP3FH.Play(0);
                    UpdateGUI();
                }
            }
        }


        /// <summary>
        /// Method to load a directory to the listbox with all the MP3 files. Creates
        /// a new Mp3Filehandler with the folder that the user wanted to play and the 
        /// GUI's media player.
        /// </summary>
        private void loadDirs()
        {
            m_mp3FileHandler = new Mp3FileHandler(fileExplorerDirectories.SelectedDir, MPlayer);
            listBoxPlayList.Items.Clear();
            listBoxPlayList.Items.AddRange(m_mp3FileHandler.GetAllInfo());
        }


        /// <summary>
        /// Method to handle the event when the user wants to play the previous song.
        /// </summary>
        /// <param name="sender">The object calling the event handler</param>
        /// <param name="e">Details concerning the event.</param>
        private void buttonPrev_Click(object sender, EventArgs e)
        {
            if (IsPlaylistInitialized())
            {
                MP3FH.PlayPrev();
                UpdateGUI();
            }
        }

        /// <summary>
        /// Method to handle the event when the user wants to play the selected song.
        /// </summary>
        /// <param name="sender">The object calling the event handler</param>
        /// <param name="e">Details concerning the event.</param>
        private void buttonPlay_Click(object sender, EventArgs e)
        {
            if (IsPlaylistInitialized())
            {
                if (isSongSelected())
                    MP3FH.Play(getSelectedSong());
                else
                    MP3FH.Play(0);
                UpdateGUI();
            }
        }


        /// <summary>
        /// Method to handle the event when the user presses the pause button and 
        /// wants to pause the song.
        /// </summary>
        /// <param name="sender">The object calling the event handler.</param>
        /// <param name="e">Details concerning the event.</param>
        private void buttonPause_Click(object sender, EventArgs e)
        {
            if (IsPlaylistInitialized())
            {
                MP3FH.PauseResume();
                UpdateGUI();
            }
        }

        /// <summary>
        /// Method to handle the event when the user presses the stop button to
        /// stop playing.
        /// </summary>
        /// <param name="sender">The object calling the event handler.</param>
        /// <param name="e">Details concerning the event.</param>
        private void buttonStop_Click(object sender, EventArgs e)
        {
            if (IsPlaylistInitialized())
            {
                MP3FH.Stop();
                UpdateGUI();
            }
        }

        /// <summary>
        /// Method to handle the event when the user presses the next button
        /// to play the next song.
        /// </summary>
        /// <param name="sender">The object calling the event handler.</param>
        /// <param name="e">Details concerning the event.</param>
        private void buttonNext_Click(object sender, EventArgs e)
        {
            if (IsPlaylistInitialized())
            {
                MP3FH.PlayNext();
                UpdateGUI();
            }
        }

        /// <summary>
        /// Handle the event when the trackbar is repositioned by the mouse.
        /// </summary>
        /// <param name="sender">The trackbar object calling the event handler.</param>
        /// <param name="e">Details concerning the event.</param>
        private void trackBarMP3_MouseCaptureChanged(object sender, EventArgs e)
        {
            MPlayer.controls.currentPosition = trackBarMP3.Value;
        }


        /// <summary>
        /// Method to update the labels in the GUI according to the state of
        /// the MP3filehandler.
        /// </summary>
        private void UpdateGUI()
        {
            if (IsPlaylistInitialized())
            {
                UpdateTitleBar();
                UpdatePlayingLabel();
                UpdateSelectedSong();
            }
            UpdateVolumeLabelImage();
        }


        /// <summary>
        /// Method to update the title bar according to the state in which the
        /// player is in.
        /// </summary>
        private void UpdateTitleBar()
        {
            if (MP3FH.PlayState == Mp3FileHandler.PlayStates.Paused)
                this.Text = "[Paused] " + MP3FH.CurrentSongString;
            else if (MP3FH.PlayState == Mp3FileHandler.PlayStates.Playing)
                this.Text = MP3FH.CurrentSongString;
            else
                this.Text = programName;
        }

        /// <summary>
        /// Method to update the currentlyplaying label to the state in which
        /// the player is in.
        /// </summary>
        private void UpdatePlayingLabel()
        {
            if (MP3FH.PlayState == Mp3FileHandler.PlayStates.Paused)
                labelNowPlaying.Text = "[Paused] " + MP3FH.CurrentSongString;
            else if (MP3FH.PlayState == Mp3FileHandler.PlayStates.Playing)
                labelNowPlaying.Text = MP3FH.CurrentSongString;
            else
                labelNowPlaying.Text = "Nothing is playing";
        }

        /// <summary>
        /// Method to update the currently selected song from the player in the
        /// playlist.
        /// </summary>
        private void UpdateSelectedSong()
        {
            if (MP3FH.PlayState == Mp3FileHandler.PlayStates.Paused)
                listBoxPlayList.SelectedIndex = MP3FH.CurrentSongIndex;
            else if (MP3FH.PlayState == Mp3FileHandler.PlayStates.Playing)
                listBoxPlayList.SelectedIndex = MP3FH.CurrentSongIndex;
            else
                listBoxPlayList.SelectedIndex = 0;
        }
                

        /// <summary>
        /// Method to validate if a song is in fact selected in the playlist
        /// </summary>
        /// <returns>true if a song is selected, else false.</returns>
        private bool isSongSelected()
        {
            return (listBoxPlayList.SelectedIndex >= 0);
        }


        /// <summary>
        /// Method to get the index for the selected song in the playlist.
        /// </summary>
        /// <returns></returns>
        private int getSelectedSong()
        {
            return listBoxPlayList.SelectedIndex;
        }


        /// <summary>
        /// Method to handle the event when the playstate of the mediaplayer
        /// has changed, to auto play next song in the playlist when one ends.
        /// 
        /// On the event of a song end, a timer is started to let the media player change
        /// state. On timer tick, its method is called to play the next song.
        /// 
        /// Also keepts track of the state of the current media to start, stop or reset
        /// the timer for the trackbar and time label.
        /// </summary>
        /// <param name="newState">New state the player has entered.</param>
        private void MPlayer_PlayStateChange(int newState)
        {
            if ((WMPLib.WMPPlayState)newState == WMPLib.WMPPlayState.wmppsMediaEnded)
            {
                TimerPlayNext.Interval = 150;
                TimerPlayNext.Enabled = true;
            }

            else if ((WMPLib.WMPPlayState)newState == WMPLib.WMPPlayState.wmppsPlaying)
            {
                trackBarMP3.Maximum = (int)(MPlayer.currentMedia.duration);
                
                TimerTrackBar.Start();
            }

            else if ((WMPLib.WMPPlayState)newState == WMPLib.WMPPlayState.wmppsPaused)
            {
                TimerTrackBar.Stop();
            }

            else if ((WMPLib.WMPPlayState)newState == WMPLib.WMPPlayState.wmppsStopped)
            {
                TimerTrackBar.Stop();
                trackBarMP3.Value = 0;
            }

        }

        /// <summary>
        /// Method to select the next item in the listbox. If the currently
        /// selected item is not the last one, the next is selected. Else
        /// the first is selected.
        /// </summary>
        private void playListSelectNext()
        {
            if (listBoxPlayList.SelectedIndex < listBoxPlayList.Items.Count - 1)
                listBoxPlayList.SelectedIndex++;
            else
                listBoxPlayList.SelectedIndex = 0;
        }

        /// <summary>
        /// Method to select the previous item in the listbox. If the currently
        /// selected index is greater than zero, the previous is selected, else
        /// nothing happens.
        /// </summary>
        private void playListSelectPrev()
        {
            if (listBoxPlayList.SelectedIndex > 0)
                listBoxPlayList.SelectedIndex--;
        }


        /// <summary>
        /// Method to handle the event when the selected index is changed.
        /// </summary>
        /// <param name="sender">The object calling the event handler.</param>
        /// <param name="e">Details concerning the event.</param>
        private void listBoxPlayList_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        /// <summary>
        /// Method to handle the event when the user double clicks within the playlist.
        /// Checks that a song is selected then calls the play-button click method,
        /// since it is treated as selection+play click.
        /// </summary>
        /// <param name="sender">The object calling the event handler.</param>
        /// <param name="e">Details concerning the event.</param>
        private void listBoxPlayList_DoubleClick(object sender, EventArgs e)
        {
            if (isSongSelected())
                this.buttonPlay_Click(sender, e);
        }


        /// <summary>
        /// Method to handle the event when the user scrolls the trackbar associated with the
        /// volume settings. Changes the volume according to the trackbar value and updates the GUI.
        /// </summary>
        /// <param name="sender">Sending object</param>
        /// <param name="e">Details associated with the event.</param>
        private void trackBarVolume_Scroll(object sender, EventArgs e)
        {
            MPlayer.settings.volume = trackBarVolume.Value;
            UpdateGUI();
        }

        /// <summary>
        /// Method to handle the event when the user clicks the shuffle label. Changes the 
        /// mode in the player and changes the image on the layer indicating if the function is active 
        /// or not.
        /// </summary>
        /// <param name="sender">Sending object</param>
        /// <param name="e">Details associated with the event.</param>
        private void labelShuffle_Click(object sender, EventArgs e)
        {
            if (IsPlaylistInitialized())
            {
                if (MP3FH.IsRandomEnabled)
                    MP3FH.IsRandomEnabled = false;
                else
                    MP3FH.IsRandomEnabled = true;
                UpdateShuffleLabelImage();
            }
        }


        /// <summary>
        /// Method to handle the event when the user clicks the repeat label. Changes the mode
        /// in the mp3filehandler and changes the image in the player, to indicate if it is triggered
        /// or not.
        /// </summary>
        /// <param name="sender">Sending object</param>
        /// <param name="e">Details associated with the event.</param>
        private void labelRepeat_Click(object sender, EventArgs e)
        {
            if (IsPlaylistInitialized())
            {
                if (MP3FH.IsRepeatEnabled)
                    MP3FH.IsRepeatEnabled = false;
                else
                    MP3FH.IsRepeatEnabled = true;
                UpdateRepeatLabelImage();
            }

        }

        /// <summary>
        /// Method to handle the event when the user drags and releases a folder on the 
        /// listbox. Checks that it is a file drop, if not a message box is shown.
        /// Checks later that the path is valid for a folder, else shows a messagebox.
        /// If multiple folders are dropped on to the window, only the first is added
        /// to the playlist.
        /// </summary>
        /// <param name="sender">Sending object</param>
        /// <param name="e">Details associated with the event.</param>
        private void listBoxPlayList_DragDrop(object sender, DragEventArgs e)
        {
            if (e.Data.GetDataPresent(DataFormats.FileDrop))
            {
                string[] FilePaths = (string[])e.Data.GetData(DataFormats.FileDrop, false);
                if (System.IO.Directory.Exists(FilePaths[0]))
                {
                    fileExplorerDirectories.RootDir = FilePaths[0];
                    fileExplorerDirectories.SelectedDir = FilePaths[0];
                    loadDirs();
                    if (listBoxPlayList.Items.Count > 0)
                        MP3FH.Play(0);
                    UpdateGUI();
                }
                else
                    MessageBox.Show("Path for the folder could not be determined");
            }
            else 
                MessageBox.Show("Only folders can be dropped!");

        }


        /// <summary>
        /// Method to handle the event when the user drags a folder on to the window. If it is a folder being
        /// dropped, show copy symbol. Else show "not applicable" symbol.
        /// </summary>
        /// <param name="sender">Sending object</param>
        /// <param name="e">Details associated with the event</param>
        private void listBoxPlayList_DragEnter(object sender, DragEventArgs e)
        {
            if (e.Data.GetDataPresent(DataFormats.FileDrop))
                e.Effect = DragDropEffects.Copy;
            else
                e.Effect = DragDropEffects.None;

        }

        /// <summary>
        /// Method to handle the event when the user drags a folder on to the window. If it is a folder being
        /// dropped, show copy symbol. Else show "not applicable" symbol.
        /// </summary>
        /// <param name="sender">Sending object</param>
        /// <param name="e">Details associated with the event.</param>
        private void MainForm_DragEnter(object sender, DragEventArgs e)
        {
            listBoxPlayList_DragEnter(sender, e);  
        }


        /// <summary>
        /// Method to handle the event when the user drops a folder on to the window.
        /// </summary>
        /// <param name="sender">The sending object</param>
        /// <param name="e">Details associated with the event</param>
        private void MainForm_DragDrop(object sender, DragEventArgs e)
        {
            listBoxPlayList_DragDrop(sender, e);
        }


        /// <summary>
        /// Method to determine if the playlist is initialized or not. I.e if the mp3-filehandler
        /// is initialized or not which indicates the same.
        /// </summary>
        /// <returns>True if the playlist is initialized, else false.</returns>
        private bool IsPlaylistInitialized()
        {
            return MP3FH != null;
        }



        /// <summary>
        /// Method to change the picture accordning to the volume level for the media
        /// player associated.
        /// </summary>
        private void UpdateVolumeLabelImage()
        {
            if (MPlayer.settings.volume == 0)
                labelVolume.Image = global::Simple_Music_Player.Properties.Resources.Volume_Off;
            else if (MPlayer.settings.volume < 25)
                labelVolume.Image = global::Simple_Music_Player.Properties.Resources.Volume_Low;
            else if (MPlayer.settings.volume < 75)
                labelVolume.Image = global::Simple_Music_Player.Properties.Resources.Volume_Medium;
            else
                labelVolume.Image = global::Simple_Music_Player.Properties.Resources.Volume_Full;
        }


        /// <summary>
        /// Method to update the image on the shuffle label so that it represents the state
        /// of the mp3filehandler.
        /// </summary>
        private void UpdateShuffleLabelImage()
        {
            if (MP3FH.IsRandomEnabled)
                labelShuffle.Image = global::Simple_Music_Player.Properties.Resources.Shuffle_enabled;
            else
                labelShuffle.Image = global::Simple_Music_Player.Properties.Resources.Shuffle_disabled;
        }

        /// <summary>
        /// Method to update the image on the repeat label so that it represents the state
        /// of the mp3filehandler.
        /// </summary>
        private void UpdateRepeatLabelImage()
        {
            if (MP3FH.IsRepeatEnabled)
                labelRepeat.Image = global::Simple_Music_Player.Properties.Resources.Repeat_Enabled;
            else
                labelRepeat.Image = global::Simple_Music_Player.Properties.Resources.Repeat_Disabled;
        }


    }
}
