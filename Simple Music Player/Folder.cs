﻿/* Class made by Christofer Reinholdsson, AC7626
 * */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Simple_Music_Player
{
    /// <summary>
    /// This class is an extension of the TreeNode class. It extends it by having extra information
    /// about the full path for the folder from its root, in addition to its name. This makes it easier
    /// to determine the full path when the folder is loaded.
    /// </summary>
    public class Folder : System.Windows.Forms.TreeNode
    {
        private string m_path;

        /// <summary>
        /// Properties for the path string. Can not be set to null, instead is set to string.empty. 
        /// </summary>
        public string Path
        {
            get { return m_path; }
            set
            {
                if (value != null)
                    m_path = value;
                else
                    m_path = string.Empty;
            }
        }

        /// <summary>
        /// Default constructor for a Folder object. Sets both the name and the path to string.empty
        /// </summary>
        public Folder()
            : this(string.Empty, string.Empty)
        { }


        /// <summary>
        /// Constructor for a folder object. Sets the name and the path to those supplied. 
        /// </summary>
        /// <param name="name">The name of the folder</param>
        /// <param name="path">The full path from the root.</param>
        public Folder(string name, string path) : base(name)
        {
            Name = name;
            Path = path;
        }

        /// <summary>
        /// Override of the toString method, returns the name of the folder.
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return Name;
        }
    }
}
